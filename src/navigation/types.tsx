import type {
  CompositeScreenProps,
  NavigatorScreenParams,
} from '@react-navigation/native';
import {NativeStackScreenProps} from '@react-navigation/native-stack';
import type {BottomTabScreenProps} from '@react-navigation/bottom-tabs';

export type RootStackParamList = {
  HomeStack: NavigatorScreenParams<HomeTabParamList>;
  BottomStack: NavigatorScreenParams<HomeTabParamList>;
};

export type RootStackScreenProps<T extends keyof RootStackParamList> =
  NativeStackScreenProps<RootStackParamList, T>;

export type HomeTabParamList = {
  Home: undefined;
};

export type BootomTabParamList = {
  Main: undefined;
  Login:undefined
};


export type HomeTabScreenProps<> =
  CompositeScreenProps<
    RootStackScreenProps<keyof RootStackParamList>,
    BottomTabScreenProps<BootomTabParamList>
  >;

declare global {
  namespace ReactNavigation {
    interface RootParamList extends RootStackParamList {}
  }
}

export interface CurrentDisplay {
  displayType: 'issues' | 'comments'
  issueId: number | null
}

export interface CurrentDisplayPayload {
  displayType: 'issues' | 'comments'
  issueId?: number
}

export interface CurrentRepo {
  org: string
  repo: string
}

export type CurrentDisplayState = {
  page: number
} & CurrentDisplay &
  CurrentRepo

  

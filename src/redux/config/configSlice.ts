import { createSlice } from '@reduxjs/toolkit'
import { Languages , Theme } from '../../enums/Config'
export const configSlice = createSlice({
  name: 'config',
  initialState: {
    language: 'en',
    theme: 'day',
    swiperindex:0
  },
  reducers: {
    changeLang: state => {
      state.language =  state.language == 'fr' ? Languages.English : Languages.French
    },
    changeTheme: state => {
      state.theme = state.theme == 'day' ? Theme.Night : Theme.Day
      
    },
    swiperChange: (state , action) => {
      state.swiperindex = action.payload
      
    },
  }
})
// Action creators are generated for each case reducer function
export const { swiperChange , changeLang , changeTheme } = configSlice.actions

export default configSlice.reducer
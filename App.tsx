// In App.js in a new project

import * as React from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { NativeStackNavigationOptions , createNativeStackNavigator } from '@react-navigation/native-stack';
import { BootomTabParamList, CurrentDisplayState, HomeTabParamList, RootStackParamList } from './src/navigation/types';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import store from './src/redux/store'
import { Provider } from 'react-redux'
import {useDispatch, useSelector} from 'react-redux';
import { displayRepo } from './src/redux/issuesDisplaySlice';
import { RootState } from './src/redux/rootReducer';
import { PersistGate } from 'redux-persist/integration/react';
import { persistStore } from 'redux-persist'

const RootStack = createNativeStackNavigator<RootStackParamList>();
const StackHome = createNativeStackNavigator<HomeTabParamList>();
const StackTab = createBottomTabNavigator<BootomTabParamList>();
const options: NativeStackNavigationOptions = {
  headerShown: false,
};

let persistor = persistStore(store);


const HomeStack = () => {
  return (
    <StackHome.Navigator  screenOptions={options}>
      <StackHome.Screen name="Home" component={Home}  />
    </StackHome.Navigator>
  );
};

const Home = ({navigation}) => {
  const test = useSelector(
    (state: RootState) => state.issuesDisplay
  )
  const dispatch = useDispatch()

  const setOrgAndRepo = (org: string, repo: string) => {
    dispatch(displayRepo({ org, repo }))
  }
  
  return (
    <TouchableOpacity onPress={()=>setOrgAndRepo('hi' , 'reza')} style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>{test?.org} {test?.repo}</Text>
    </TouchableOpacity>
  );
}

const Main = () => {

  return (
    <View  style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Main Screen</Text>
    </View>
  );
}

const Login = () => {

  return (
    <View  style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
      <Text>Login Screen</Text>
    </View>
  );
}



const BottomStack = () => {

  return (
    <StackTab.Navigator>
      <StackTab.Screen name="Main" component={Main} />
      <StackTab.Screen name="Login" component={Login} />
    </StackTab.Navigator>
  );
}



const App = () => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
      <NavigationContainer>
        <RootStack.Navigator screenOptions={options} >
          <RootStack.Screen name="HomeStack" component={HomeStack} />
          <RootStack.Screen name="BottomStack" component={BottomStack} />
        </RootStack.Navigator>
      </NavigationContainer>
      </PersistGate>
    </Provider>
  );
}

export default App;